package com.automationanywhere.sample;


 //import org.jbpm.process.workitem.AbstractLogOrThrowWorkItemHandler;
 import org.jbpm.bpmn2.handler.ServiceTaskHandler;
 import org.jbpm.process.workitem.AbstractWorkItemHandler;
 //import org.jbpm.process.workitem.bpmn2.ServiceTaskHandler;

 import org.jbpm.process.workitem.handler.*;
 import org.jbpm.process.workitem.handler.JavaHandlerWorkItemHandler;
 import org.jbpm.workflow.instance.impl.WorkflowProcessInstanceImpl;
 import org.json.simple.JSONArray;
 import org.json.simple.JSONObject;
 import org.json.simple.parser.JSONParser;
 import org.apache.http.HttpResponse;
 import org.apache.http.client.HttpClient;
 import org.apache.http.client.methods.HttpPost;
 import org.apache.http.entity.ContentType;
 import org.apache.http.entity.StringEntity;
 //import org.apache.http.impl.client.DefaultHttpClient;
 import org.apache.http.impl.client.HttpClientBuilder;
 import org.apache.http.util.EntityUtils;
 import org.jbpm.ruleflow.core.RuleFlowProcess;
 import org.jbpm.workflow.core.node.HumanTaskNode;
 import org.kie.api.KieBase;
 import org.kie.api.definition.process.Node;
 import org.kie.api.definition.process.Process;
 import org.kie.api.io.ResourceType;
 import org.kie.api.runtime.KieSession;
 import org.kie.api.runtime.process.ProcessInstance;
 import org.kie.api.runtime.process.WorkItem;
 import org.kie.api.runtime.process.WorkItemHandler;
 import org.kie.api.runtime.process.WorkItemManager;
 import org.kie.internal.io.ResourceFactory;

 import org.kie.internal.runtime.StatefulKnowledgeSession;
 import org.kie.internal.runtime.manager.context.ProcessInstanceIdContext;
 import org.kie.internal.task.api.UserGroupCallback;
 //import org.kie.internal.utils.KieService;

 import java.util.*;
 import java.io.*;
 import org.kie.api.runtime.manager.*;

 import org.jbpm.runtime.manager.impl.RuntimeEnvironmentBuilder;
 import org.jbpm.runtime.manager.impl.RuntimeManagerFactoryImpl;
 import org.jbpm.services.task.identity.JBossUserGroupCallbackImpl;
 import org.jbpm.test.JBPMHelper;


 /**
  * Example that uses the remote Java Client (through REST) to connect to the execution server
  * of the jBPM Console and execute the Evaluation process there.
  *
  * This example assumes:
  *  - you have the jbpm console running at http://localhost:8080/jbpm-console
  *    (automatically when using jbpm-installer)
  *  - you have users krisv/krisv, john/john and mary/mary
  *    (automatically when using jbpm-installer)
  *  - you have deployed the Evaluation project (part of the jbpm-playground)
  *
  */
 public class Sample {

     public static void main(String[] args) throws Exception {

         //File file = new File("C:\\jBPM\\testAA\\src\\main\\resources\\testAA.bpmn2");
         //File file = new File("C:\\jBPM\\jbpm-installer-7.6.0.Final\\.kie-wb-playground\\itorders\\src\\main\\resources\\org\\jbpm\\demo\\itorders\\orderhardware.bpmn2");
        // File file = new File("C:\\project\\bpmnWithBots.bpmn");
         //File file = new File("C:\\project\\AASample_fromIBM\\AASample_kk-10000a79db2a0e4\\AASample_kk.bpmn");
         File file = new File("C:\\project\\AABOT.bpmn2");
         //File file = new File("C:\\project\\callScript.bpmn2");

         if(file == null || !file.exists()) {

             throw new FileNotFoundException("Specified file path is invalid.");

         }

         // create the entity manager factory and register it in the environment

 //        EntityManagerFactory emf =

   //             Persistence.createEntityManagerFactory( "org.jbpm.domain" );

         /* newEmptyBuilder */
         /*
         JBPMHelper.startH2Server();
         JBPMHelper.setupDataSource();
         Properties properties= new Properties();
         properties.setProperty("krisv", "admin");
         UserGroupCallback userGroupCallback = new JBossUserGroupCallbackImpl(properties);
*/
         RuntimeEnvironment environment = RuntimeEnvironmentBuilder.Factory.get().newEmptyBuilder()

                 .addAsset(ResourceFactory.newFileResource(file ), ResourceType.BPMN2)

                 .get();
         //environment.usePersistence();
         //environment.getRegisterableItemsFactory();

         RuntimeManager manager = RuntimeManagerFactory.Factory.get().newSingletonRuntimeManager(environment);
         //RuntimeManager manager = getRuntimeManager(file);
         RuntimeEngine runtime = manager.getRuntimeEngine(ProcessInstanceIdContext.get());



         KieSession ksession = runtime.getKieSession();

         KieBase kbase = ksession.getKieBase();
         Collection<Process> processes = kbase.getProcesses();
         Iterator<Process> iterator = processes.iterator();
         String processName = new String();
         if (iterator.hasNext()) {
             Process p = iterator.next();
             processName = p.getId();
         }

//         TaskService ts = runtime.getTaskService();


         ksession.getWorkItemManager().registerWorkItemHandler("User", new JavaHandlerWorkItemHandler((StatefulKnowledgeSession)ksession) {
             public void executeWorkItem(WorkItem workItem, WorkItemManager workItemManager) {
                 // call the BOT
                 System.out.println("Calling BOT ...");
             }
             public void abortWorkItem(WorkItem workItem, WorkItemManager workItemManager) {
                 // abort the BOT
                 System.out.println("Aborting BOT ...");
             }
         });

         ksession.getWorkItemManager().registerWorkItemHandler("Human Task", new JavaHandlerWorkItemHandler((StatefulKnowledgeSession)ksession) {
             public void executeWorkItem(WorkItem workItem, WorkItemManager workItemManager) {
                 // call the BOT
                 System.out.println("Calling BOT ...(Human)");
             }
             public void abortWorkItem(WorkItem workItem, WorkItemManager workItemManager) {
                 // abort the BOT
                 System.out.println("Aborting BOT ...");
             }
         });


         ksession.getWorkItemManager().registerWorkItemHandler("Abstract Task", new AbstractWorkItemHandler((StatefulKnowledgeSession) ksession) {
             public void executeWorkItem(WorkItem workItem, WorkItemManager workItemManager) {
                 // call the BOT
                 System.out.println("Calling Script ...(Abstract)");
             }

             public void abortWorkItem(WorkItem workItem, WorkItemManager workItemManager) {
                 // abort the BOT
                 System.out.println("Aborting Script ...");
             }
         });

         ksession.getWorkItemManager().registerWorkItemHandler(
                 "AABOTTaskHandler",
                 new AABOTTaskHandler()
         );

         ksession.getWorkItemManager().registerWorkItemHandler("Service", new ServiceTaskHandler() {
             public void executeWorkItem(WorkItem workItem, WorkItemManager workItemManager) {
                 // call the BOT
                 System.out.println("Calling Script ...(Service)");
             }

             public void abortWorkItem(WorkItem workItem, WorkItemManager workItemManager) {
                 // abort the BOT
                 System.out.println("Aborting Script ...");
             }
         });


             ProcessInstance pi = null;
         try {
             // pi =  ksession.startProcess("testAA"); /* itorders.orderhardware */
             //pi =  ksession.startProcess("itorders.orderhardware");
             pi =  ksession.startProcess(processName);
         }
         catch (Throwable t) {
             t.printStackTrace();
         }

         if (!(pi instanceof WorkflowProcessInstanceImpl))
         {
             System.out.println("Not a valid Workflow");
             System.exit(0);
         }

         RuleFlowProcess rfp = (RuleFlowProcess) pi.getProcess();
         Node[] nodes =  rfp.getNodes();

         Node node = null;
         boolean calledBOT = false;
         for (int i=0; i< nodes.length ; i++)
         {
             node = nodes[i];

             Map nm = node.getMetaData();
             if (nm.get("bot") != null)
             {
                 String botFile = (String) nm.get("bot");
                 String botId = (String) nm.get("botId");
                 System.out.println("Bot ID is : " + botId + " and Bot File is : " + botFile);
                 //Call BOT
                 if (!calledBOT) {
                     HttpClient client = HttpClientBuilder.create().build();
                     HttpResponse response = null;
                     try
                     {
                         calledBOT = true;
                         String auth = null;
                         HttpPost getAuthToken = new HttpPost("http://ec2-50-112-20-116.us-west-2.compute.amazonaws.com:80/v1/authentication");


                        // for the moment hardcode the authentication and BOT call - read it from a DB later.

                         getAuthToken.setHeader("Content-type", "application/json");
                         getAuthToken.setHeader("accept", "application/json");
                         String sparams = "{ \"username\":\"admin\",\"password\": \"Automation\"}";
                         System.out.println("Length of sparams for Auth call is : " + sparams.length() );
                         StringEntity params = new StringEntity(sparams,ContentType.APPLICATION_FORM_URLENCODED);
                         getAuthToken.setEntity(params);
                         response = client.execute(getAuthToken);

                         String json = EntityUtils.toString(response.getEntity(), "UTF-8");
                         try {
                             JSONParser parser = new JSONParser();
                             Object resultObject = parser.parse(json);

                            if (resultObject instanceof JSONObject) {
                                 JSONObject obj =(JSONObject)resultObject;
                                 auth = (String) obj.get("token");
                                 System.out.println("Auth from auth call : " + auth);

                             }

                         } catch (Exception e) {
                             System.out.println(e.getMessage());
                         }

                         HttpPost request = new HttpPost("http://ec2-50-112-20-116.us-west-2.compute.amazonaws.com:80/v1/schedule/automations/deploy");
                         sparams ="{ \"taskRelativePath\": \"My Tasks\\\\dirtest.atmx\", \"botRunners\": [ { \"client\": \"AA-SJ-KrishnaK.AAI.AASPL-BRD.COM\", \"user\": \"kk\" } ], \"runWithRDP\": false } ";
                         System.out.println("Length of sparams = " + sparams.length());
                         params = new StringEntity(sparams,ContentType.APPLICATION_FORM_URLENCODED);
                         System.out.println(params.toString());
                         request.setHeader("X-Authorization",auth);

                         request.setHeader("Content-type", "application/json");
                         System.out.println("Auth sent to deploy call : " + auth);
                         request.setEntity(params);
                         response = client.execute(request);

                         json = EntityUtils.toString(response.getEntity(), "UTF-8");
                         try {
                             JSONParser parser = new JSONParser();
                             JSONObject resultObject = (JSONObject) parser.parse(json);
                             System.out.println(resultObject.get("message"));

                         } catch (Exception e) {
                             System.out.println(e.getMessage());
                         }
                         System.out.println(response.toString());
                     }
                     catch (Exception e) {
                         System.out.println(e.getMessage());
                         System.out.println(response.toString());
                     }
                  }
             }
         }


         System.out.println("Process Name : " + pi.getProcessName()+  " Status : "+  pi.getState());

         //TaskSummaryList tsl = pi.getActiveUserTasks();

        //InternalTaskService taskService = null;
        //SynchronizedRuntimeImpl  srtI = new  SynchronizedRuntimeImpl( ksession,  taskService);


         //try {
         //     taskService =  runtime.getTaskService();
         //}
         //catch (Throwable t) {
         //    t.printStackTrace();
         //}
         //String TASK_USER="krisv";
         //List tasklist = taskService.getTasksAssignedAsPotentialOwner( TASK_USER, "en-UK");

         //long taskId = ((Long) tasklist.get(0)).longValue();

         //taskService.claim(taskId, TASK_USER);
         //taskService.start(taskId, TASK_USER);
         //taskService.complete(taskId, TASK_USER, null);
         //System.out.println("Process instance completed");

         //ksession.startProcess("testAA");

         System.out.println("Proc started!");






         //COMMENT
         /*

         KieServicesConfiguration config = KieServicesFactory.newRestConfiguration(
                 "http://localhost:8080/kie-server/services/rest/server", "krisv", "krisv");
         KieServicesClient client = KieServicesFactory.newKieServicesClient(config);
         ProcessServicesClient processServices = client.getServicesClient(ProcessServicesClient.class);
         UserTaskServicesClient taskServices = client.getServicesClient(UserTaskServicesClient.class);


         ProcessInstance pInst = processServices.getProcessInstance("itorders_1.0.0-SNAPSHOT", new Long(12));
         // start a new process instance
         Map<String, Object> params = new HashMap<String, Object>();
         params.put("approved", true);

         List stat = new ArrayList();

         stat.add(Status.InProgress);
         stat.add(Status.Reserved);
         stat.add(Status.Ready);

         List<TaskSummary> tlist = taskServices.findTasksByStatusByProcessInstanceId(pInst.getId(),stat,0,10 );
         Long processInstanceId = pInst.getId();//processServices.startProcess("itorders_1.0.0-SNAPSHOT", "itorders-data.place-order", params);
         System.out.println("Start Evaluation process " + processInstanceId);

         // complete Self Evaluation
         //List <TaskSummary> tsks = taskServices.findTasks("krisv",0,10);
         //List<TaskSummary> tasks = taskServices.findTasksAssignedAsPotentialOwner("krisv", 0, 10);
         TaskSummary task = findTask(tlist, processInstanceId);
         System.out.println("'mary' completing task " + task.getName() + ": " + task.getDescription());
         taskServices.startTask("itorders_1.0.0-SNAPSHOT", task.getId(), "mary");
         Map<String, Object> results = new HashMap<String, Object>();
         results.put("ordered", false);
         results.put("info", "Rejected Request");
         taskServices.completeTask("itorders_1.0.0-SNAPSHOT", task.getId(), "mary", results);
         */
         // END COMMENT

/*
         // john from HR
         config = KieServicesFactory.newRestConfiguration(
                 "http://localhost:8080/kie-server/services/rest/server", "john", "john");
         client = KieServicesFactory.newKieServicesClient(config);
         taskServices = client.getServicesClient(UserTaskServicesClient.class);

         tasks = taskServices.findTasksAssignedAsPotentialOwner("john", 0, 10);
         task = findTask(tasks, processInstanceId);
         System.out.println("'john' completing task " + task.getName() + ": " + task.getDescription());
         taskServices.claimTask("evaluation_1.0.0-SNAPSHOT", task.getId(), "john");
         taskServices.startTask("evaluation_1.0.0-SNAPSHOT", task.getId(), "john");
         results = new HashMap<String, Object>();
         results.put("performance", "9");
         taskServices.completeTask("evaluation_1.0.0-SNAPSHOT", task.getId(), "john", results);

         // mary from PM
         config = KieServicesFactory.newRestConfiguration(
                 "http://localhost:8080/kie-server/services/rest/server", "mary", "mary");
         client = KieServicesFactory.newKieServicesClient(config);
         taskServices = client.getServicesClient(UserTaskServicesClient.class);

         tasks = taskServices.findTasksAssignedAsPotentialOwner("mary", 0, 10);
         task = findTask(tasks, processInstanceId);
         System.out.println("'mary' completing task " + task.getName() + ": " + task.getDescription());
         taskServices.claimTask("evaluation_1.0.0-SNAPSHOT", task.getId(), "mary");
         taskServices.startTask("evaluation_1.0.0-SNAPSHOT", task.getId(), "mary");
         results = new HashMap<String, Object>();
         results.put("performance", "10");
         taskServices.completeTask("evaluation_1.0.0-SNAPSHOT", task.getId(), "mary", results);

         QueryServicesClient queryServices = client.getServicesClient(QueryServicesClient.class);
         ProcessInstance processInstance = (ProcessInstance) queryServices.findProcessInstanceById(processInstanceId);
         if (processInstance.getState() != 2) {
             throw new RuntimeException("Process instance not completed");
         }
         System.out.println("Process instance completed");
         */
     }
//COMMENT
     /*
     private static TaskSummary findTask(List<TaskSummary> tasks, long processInstanceId) {
         for (TaskSummary task : tasks) {
             if (task.getProcessInstanceId() == processInstanceId) {
                 return task;
             }
         }
         throw new RuntimeException("Could not find task for process instance "
                 + processInstanceId + " [" + tasks.size() + " task(s) in total]");
     }
*/
// END COMMENT


private static RuntimeManager getRuntimeManager(File file ) {
    // load up the knowledge base
   // JBPMHelper.startH2Server();
    JBPMHelper.startH2Server();
    //JBPMHelper.setupDataSource();
    Properties properties= new Properties();
    properties.setProperty("krisv", "admin");
    UserGroupCallback userGroupCallback = new JBossUserGroupCallbackImpl(properties);
    RuntimeEnvironment environment = RuntimeEnvironmentBuilder.getDefault()
            .userGroupCallback(userGroupCallback)
            .addAsset(ResourceFactory.newFileResource(file ), ResourceType.BPMN2 )
            .get();
    RuntimeManagerFactoryImpl factory = new RuntimeManagerFactoryImpl();
    return factory.newSingletonRuntimeManager(environment);
}

 }
