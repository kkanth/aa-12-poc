package com.automationanywhere.sample;



import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;


import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;


public class BOTServiceCamunda implements JavaDelegate
 {
     //public static BOTService INST = new BOTService();
     /*public static BOTService getInstance()
     {
        return INST;
     }
     */
     public void execute(DelegateExecution de)
     {
         String botName = (String)de.getVariable("BOT_Name");
         System.out.println("Hi " + botName + ", How are you doing today? ");

         HttpClient client = HttpClientBuilder.create().build();
         HttpResponse response = null;
         boolean error = false;
         try
         {
             String auth = null;
             HttpPost getAuthToken = new HttpPost("http://ec2-50-112-20-116.us-west-2.compute.amazonaws.com:80/v1/authentication");


             // for the moment hardcode the authentication and BOT call - read it from a DB later.

             getAuthToken.setHeader("Content-type", "application/json");
             getAuthToken.setHeader("accept", "application/json");
             String sparams = "{ \"username\":\"admin\",\"password\": \"Automation\"}";
             System.out.println("Length of sparams for Auth call is : " + sparams.length() );
             StringEntity params = new StringEntity(sparams,ContentType.APPLICATION_FORM_URLENCODED);
             getAuthToken.setEntity(params);
             response = client.execute(getAuthToken);


             String json = EntityUtils.toString(response.getEntity(), "UTF-8");
             try {
                 JSONParser parser = new JSONParser();
                 Object resultObject = parser.parse(json);

                 if (resultObject instanceof JSONObject) {
                     JSONObject obj =(JSONObject)resultObject;
                     auth = (String) obj.get("token");
                     System.out.println("Auth from auth call : " + auth);

                 }

             } catch (Exception e) {
                 System.out.println(e.getMessage());
             }

             HttpPost request = new HttpPost("http://ec2-50-112-20-116.us-west-2.compute.amazonaws.com:80/v1/schedule/automations/deploy");
             sparams ="{ \"taskRelativePath\": \"My Tasks\\\\" + botName + "\" , \"botRunners\": [ { \"client\": \"AA-SJ-KrishnaK.AAI.AASPL-BRD.COM\", \"user\": \"kk\" } ], \"runWithRDP\": false } ";
             //sparams ="{ \"taskRelativePath\": \"My Tasks\\\\dirtest.atmx\", \"botRunners\": [ { \"client\": \"AA-SJ-KrishnaK.AAI.AASPL-BRD.COM\", \"user\": \"kk\" } ], \"runWithRDP\": false } ";
             System.out.println("Length of sparams = " + sparams.length());
             params = new StringEntity(sparams,ContentType.APPLICATION_FORM_URLENCODED);
             System.out.println(sparams);
             request.setHeader("X-Authorization",auth);

             request.setHeader("Content-type", "application/json");
             System.out.println("Auth sent to deploy call : " + auth);
             request.setEntity(params);
             response = client.execute(request);

             json = EntityUtils.toString(response.getEntity(), "UTF-8");
             try {
                 JSONParser parser = new JSONParser();
                 JSONObject resultObject = (JSONObject) parser.parse(json);
                 System.out.println(resultObject.get("message"));

             } catch (Exception e) {
                 System.out.println(e.getMessage());
             }
             System.out.println(response.toString());
         }
         catch (Exception e) {
             System.out.println(e.getMessage());
             error = true;
         }
         if (!error)
            de.setVariable("BOT_Status", "Succes");
         else
             de.setVariable("BOT_Status","Failed");
     }
 }
